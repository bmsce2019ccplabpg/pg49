#include <stdio.h>

int main()
{
    struct book 
    {
        char name[50];
        int number;
        char author[20];
        struct date
        {
            int dd,mm,yy;
        }doi,dor;
    }b={"Paper Towns",12788,"John Green",11,11,2019,18,11,2019};
    
    printf("\nLibrary book details are : ");
    
    printf("\n Book name : %s\n Book number : %d\n Book author : %s\n Date of issue : %d.%d.%d\n Date of return : %d.%d.%d\n",b.name,b.number,b.author,b.doi.dd,b.doi.mm,b.doi.yy,b.dor.dd,b.dor.mm,b.dor.yy);
    
    return 0;   

}