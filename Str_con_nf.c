#include <stdio.h>
int main()
{
    char s1[10], s2[10], i, j;
    printf("Enter first string:\n ");
    scanf("%s", s1);
    
    printf("Enter second string:\n ");
    scanf("%s", s2);
  
    for(i = 0; s1[i] != '\0'; i++);
    for(j = 0; s2[j] != '\0'; j++, i++)
    {
        s1[i] = s2[j];
    }
    s1[i] = '\0';
    printf("\n After concatenation: %s", s1);
    return 0;
}
