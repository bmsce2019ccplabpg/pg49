#include<stdio.h> 
int main()
{
    int marks[5][3], i,j,max;
    for(i=0;i<5;i++)
    {
        for(j=0;j<3;j++)
        {
            printf("Enter the marks of student %d in subject %d\n",i+1,j+1);
            scanf("%d", &marks[i][j]);
        }
    }

    for(j=0;j<3;j++)
    {
        max=marks[0][j];
        for(i=0;i<5;i++)
        {
            if(marks[i][j]>=max)
            {
                max=marks[i][j];
            }

        }
        printf("\n The max marks in subject %d is %d", j+1,max);

    }

    return 0;

}