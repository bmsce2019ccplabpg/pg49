#include <stdio.h>
#include <math.h>
struct point
{
	float x,y;
};
float compute_distance(struct point a , struct point b)
{
	float d;
	d= sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
	return d;
}

int main(void)
{
    struct point a,b;
    float d;
    printf("Enter the coordinates of point a");
    scanf("%f %f",&a.x,&a.y);
    printf("Enter the coordinates of point b");
    scanf("%f %f",&b.x,&b.y);
    d=compute_distance(a,b);
    printf("d=%f",d);
}