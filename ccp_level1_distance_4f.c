#include <stdio.h>
#include <math.h>
float input1()
{
	float x1;
	printf("Enter the first X coordinate");
	scanf("%f",&x1);
	return x1;
}
float input2()
{
	float y1;
	printf("Enter the first Y coordinate");
	scanf("%f",&y1);
	return y1;
}
float input3()
{
	float x2;
	printf("Enter the second X coordinate");
	scanf("%f",&x2);
	return x2;
}
float input4()
{
	float y2;
	printf("Enter the second y coordinate");
	scanf("%f",&y2);
	return y2;
}
float compute(float x1,float y1,float x2,float y2)
{
	float dist;
	dist=sqrt((x2-x1)*(x2-x1)-(y2-y1)*(y2-y1));
	return dist;
}
void output()
{
	float dist;
	printf("The dist=%f",dist);
}
int main()
{
	float x1,y1,x2,y2,c;
	x1=input1();
	y1=input2();
	x2=input3();
	y2=input4();
	c=compute(x1,y1,x2,y2);
	output(c);
	return 0;
}