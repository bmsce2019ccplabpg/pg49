#include <stdio.h> 
 
int main()
{
   int i,h,l,m,key,a[5],n;
 
   printf("Enter number of elements\n");
   scanf("%d",&n);
 
   printf("Enter %d integers\n", n);
 
   for (i = 0; i < n; i++)
      scanf("%d",&a[i]);
 
   printf("Enter value to find\n");
   scanf("%d", &key);
 
   l=0;
   h=n-1;
   m=(l+h)/2;
 
   while (l<=h) 
   {
      if (a[m]<key)
         l=m+1;    
      else if (a[m]==key)
      {
         printf("%d found at location %d.\n", key,m+1);
         break;
      }
      else
         h=m-1;
 
      m=(l+h)/2;
   }
   if (l>h)
      printf("Element not found\n", key);
 
   return 0;  
}